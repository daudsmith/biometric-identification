ABOUT
-----------------------------------------------------------------------------

This Source for investigating new modalities, improving existing algorithms
interfacing with commercial systems, measuring recognition performance, and deploying automated biometric systems
The project is designed to facilitate rapid algorithm prototyping, and features a mature core framework
flexible plugin system, and support for open and closed source development.
Off-the-shelf algorithms are also available for specific modalities including face recognition, age estimation, and gender estimation.

Biometric Identification originated within OpenBR & The MITRE Corporation from a need to streamline the process of prototyping new algorithms.
The project was later published as open source software under the Apache 2 license and is free for academic and commercial use.



BUILD & INSTALL
----------------------------------------------------------------------

Biometric Identification is supported on Windows, Mac OS X, and Debian Linux. 
The project is licensed under Apache 2.0 and releases follow the Semantic Versioning convention.
Internally the code base uses the CMake build system and requires Qt and OpenCV.


Linux
Install GCC 4.9.2

$ sudo apt-get update
$ sudo apt-get install build-essential
Install CMake 3.0.2

$ sudo apt-get install cmake cmake-curses-gui
Download OpenCV 2.4.11, note Build OpenCV with video support

$ cd ~/Downloads
$ unzip opencv-2.4.11.zip
$ cd opencv-2.4.11
$ mkdir build
$ cd build
$ cmake -DCMAKE_BUILD_TYPE=Release ..
$ make -j4
$ sudo make install
$ cd ../..
$ rm -rf opencv-2.4.11*
Install Qt 5.4.1

$ sudo apt-get install qt5-default libqt5svg5-dev qtcreator
Create a GitHub account, follow their instructions for setting up Git.

$ git clone https://gitlab.com/daudyusuf/biometric-identification.git

Build biometric-identification!

$ mkdir build # from the biometric-identification root directory
$ cd build
$ cmake -DCMAKE_BUILD_TYPE=Release ..
$ make -j4
$ sudo make install


Open Qt Creator IDE

$ qtcreator &

From the Qt Creator "File" menu select "Open File or Project...".

Select "biometric-identification/CMakeLists.txt" then "Open".
Browse to your pre-existing build directory "biometric-identification/build" then select "Next".
Select "Run CMake" then "Finish".
You're all set! You can find more information on Qt Creator here if you need it.
(Optional) Test biometric-identification!

$ cd biometric-identification/scripts
$ ./downloadDatasets.sh
$ cd ../build
$ make test
(Optional) Package biometric-identification!

$ cd biometric-identification/build
$ sudo cpack -G TGZ
(Optional) Build biometric-identification documentation!

Build the docs

$ pip install mkdocs
$ cd biometric-identification/docs
$ sh build_docs.sh
$ mkdocs serve
Navigate to http://127.0.0.1:8000 in your browser to view the docs.

OSX
Download and install the latest "XCode" and "Command Line Tools" from the Apple Developer Downloads page.

Download CMake 3.0.2

    $ cd ~/Downloads
    $ tar -xf cmake-3.0.2.tar.gz
    $ cd cmake-3.0.2
    $ ./configure
    $ make -j4
    $ sudo make install
    $ cd ..
    $ rm -rf cmake-3.0.2*
Download OpenCV 2.4.11

$ cd ~/Downloads
$ unzip opencv-2.4.11.zip
$ cd opencv-2.4.11
$ mkdir build
$ cd build
$ cmake -DCMAKE_BUILD_TYPE=Release ..
$ make -j4
$ sudo make install
$ cd ../..
$ rm -rf opencv-2.4.11*
Download and install Qt 5.4.1

Create a GitHub account, follow their instructions for setting up Git.

$ git clone https://gitlab.com/daudyusuf/biometric-identification.git

Build biometric-identification!

$ mkdir build # from the biometric-identification root directory
$ cd build
$ cmake -DCMAKE_PREFIX_PATH=~/Qt/5.4.1/clang_64 -DCMAKE_BUILD_TYPE=Release ..
$ make -j4
$ sudo make install


Open Qt Creator IDE

$ open ~/Qt/Qt\ Creator.app
From the Qt Creator "File" menu select "Open File or Project...".

Select "biometric-identification/CMakeLists.txt" then "Open".
Browse to your pre-existing build directory "biometric-identification/build" then select "Continue".
Select "Run CMake" then "Done".
You're all set! You can find more information on Qt Creator here if you need it.
(Optional) Test biometric-identification!

$ cd biometric-identification/scripts
$ ./downloadDatasets.sh
$ cd ../build
$ make test
(Optional) Package biometric-identification!

$ cd biometric-identification/build
$ sudo cpack -G TGZ
(Optional) Build biometric-identification documentation!

Build the docs

$ pip install mkdocs
$ cd biometric-identification/docs
$ sh build_docs.sh
$ mkdocs serve
Navigate to http://127.0.0.1:8000 in your browser to view the docs.

Windows
Download Visual Studio Express 2013 for Windows Desktop and install. You will have to register with Microsoft, but it's free.

Download and Install CMake 3.0.2

During installation setup select "Add CMake to PATH".
Download OpenCV 2.4.11

Consider the free open source program 7-Zip if you need a program to unarchive tarballs.
Move the "opencv-2.4.11" folder to "C:\".
Open "VS2013 x64 Cross Tools Command Prompt" (from the Start Menu, select "All Programs" -> "Microsoft Visual Studio 2013" -> "Visual Studio Tools" -> "VS2013 x64 Cross Tools Command Prompt") and enter:
$ cd C:\opencv-2.4.11
$ mkdir build-msvc2013
$ cd build-msvc2013
$ cmake -G "NMake Makefiles" -DBUILD_PERF_TESTS=OFF -DBUILD_TESTS=OFF -DWITH_FFMPEG=OFF -DCMAKE_BUILD_TYPE=Debug ..
$ nmake
$ nmake install
$ cmake -DCMAKE_BUILD_TYPE=Release ..
$ nmake
$ nmake install
$ nmake clean
Download and Install Qt 5.4.1

Create a GitHub account and follow their instructions for setting up Git.

Launch "Git Bash" from the Desktop and clone biometric-identification:
$ cd /c
$ git clone https://gitlab.com/daudyusuf/biometric-identification.git

Build biometric-identification!

From the VS2013 x64 Cross Tools Command Prompt:

$ cd C:\biometric-identification
$ mkdir build-msvc2013
$ cd build-msvc2013
$ cmake -G "CodeBlocks - NMake Makefiles" -DCMAKE_PREFIX_PATH="C:/opencv-2.4.11/build/install;C:/Qt/Qt5.4.1/5.4/msvc2013_64" -DCMAKE_INSTALL_PREFIX="./install" -DBR_INSTALL_DEPENDENCIES=ON -DCMAKE_BUILD_TYPE=Release ..
$ nmake
$ nmake install
Check out the "install" folder.



From the VS2013 x64 Cross Tools Command Prompt: $ C:\Qt\Qt5.4.1\Tools\QtCreator\bin\qtcreator.exe
From the Qt Creator "Tools" menu select "Options..."
Under "Kits" select "Desktop (default)"
For "Compiler:" select "Microsoft Visual C++ Compiler 11.0 (x86_amd64)" and click "OK"
From the Qt Creator "File" menu select "Open File or Project...".
Select "C:\biometric-identification\CMakeLists.txt" then "Open".
If prompted for the location of CMake, enter "C:\Program Files (x86)\CMake 3.0.2\bin\cmake.exe".
Browse to your pre-existing build directory "C:\biometric-identification\build-msvc2013" then select "Next".
Select "Run CMake" then "Finish".
You're all set! You can find more information on Qt Creator here if you need.
(Optional) Package biometric-identification!

From the VS2013 x64 Cross Tools Command Prompt: $ cd C:\biometric-identification\build-msvc2013 $ cpack -G ZIP
Raspbian
Install CMake 2.8.9

$ sudo apt-get install cmake
Download OpenCV 2.4.9

$ wget http://sourceforge.net/projects/opencvlibrary/files/opencv-unix/2.4.9/opencv-2.4.9.zip
$ unzip opencv-2.4.9.zip
$ cd opencv-2.4.9
$ mkdir build
$ cd build
$ cmake -DCMAKE_BUILD_TYPE=Release ..
$ make
$ sudo make install
$ cd ../..
$ rm -rf opencv-2.4.9*
Install Qt5

Modify source list

$ nano /etc/apt/sources.list
by changing:

$ deb http://mirrordirector.raspbian.org/raspbian/ wheezy main contrib non-free rpi
to:

$ deb http://mirrordirector.raspbian.org/raspbian/ jessie main contrib non-free rpi
Update apt-get

$ sudo apt-get update
Install packages

$ sudo apt-get install qt5-default libqt5svg5-dev
Create a GitHub account, follow their instructions for setting up Git.

$ git clone https://gitlab.com/daudyusuf/biometric-identification.git

Build biometric-identification!

$ mkdir build # from the biometric-identification root directory
$ cd build
$ cmake -DCMAKE_BUILD_TYPE=Release ..
$ make
$ sudo make install
(Optional) Test biometric-identification!

$ cd biometric-identification/scripts
$ ./downloadDatasets.sh
$ cd ../build
$ make test


DEVELOPER
--------------------------------------------------------------------------------
Thanks to Developers

Josh Klontz, M. Taborsky, Charles Otto, Brendan Klare, Scott Klum and Austin Blanton.